"""A Text Adventure Game about exploring a dungeon."""

import random
import math
import copy
import names

HELP_MESSAGE = """Commands: m (move), q (quit), l (look), h (help).
You can also type the letter of an exit to move through it.\n"""

continue_game = True


def randomChoicesUnique(seq, n: int):
    """Return a list up to n in size of population elements
    chosen without replacement.
    This means the same element is not chosen multiple times."""
    # Without repetition, the entire list will be returned.
    if len(seq) <= n:
        # A new object is generated in the default case,
        # so we create a copy for consistent behaviour.
        return copy.copy(seq)

    choices = []
    while len(choices) < n:
        selection = random.choice(seq)
        if selection not in choices:
            choices.append(selection)

    return choices


def randomChance(probability: float) -> bool:
    """Return whether an event with the given probability occurs,
    randomly determined with random.random()."""
    return probability > random.random()


class Room:
    pass


class Dungeon:
    pass


class RoomLayout:
    """The layout of the rooms in a dungeon."""
    rooms: list[Room]

    def __init__(self, max_depth: int, door_pairs):
        """Generate the dungeon layout."""
        self.rooms = []
        self.max_depth = max_depth
        self.door_pairs = door_pairs
        self.door_directions = copy.copy(list(door_pairs.keys()))
        self.max_doors = len(self.door_directions)

        # First room
        self.rooms.append(Room(0, self,
                               None, random.choice(self.door_directions)))

        previous = 0

        while len(self.rooms) < math.ceil(max_depth * 2.5):
            self.addRoom(previous)

            previous = self.selectPrevious()

        self.addCrossLinks()

        self.removeUnusedDoors()

    def addCrossLinks(self):
        """Add connections between 'None' doors in existing rooms."""
        available = self.roomsWithEmptyDoors()
        while len(available) > 1:
            ids = randomChoicesUnique(available, 2)
            self.rooms[ids[0]].addRandomConnection(ids[1])
            self.rooms[ids[1]].addRandomConnection(ids[0])
            available = self.roomsWithEmptyDoors()

    def removeUnusedDoors(self):
        """Remove all doors connecting to 'None' in the dungeon."""
        # This call may be unnecessary.
        for roomKey in self.roomsWithEmptyDoors():
            emptyExits = self.rooms[roomKey].availableExitKeys()
            for exit in emptyExits:
                del self.rooms[roomKey].exits[exit]

    def selectPrevious(self) -> int:
        """Select the 'previous' room: the one to attach the next one to."""
        # It's easiest to continue from the most recent room, if possible.
        if len(self.rooms[-1].availableExitKeys()) > 0:
            return len(self.rooms) - 1
        else:
            return self.firstIncompleteRoom()

    def firstIncompleteRoom(self):
        """Find the first room in the dungeon with a 'None' door."""
        for room in self.rooms:
            if len(room.availableExitKeys()) > 0:
                return self.rooms.index(room)

    def linearDeepestIncompleteRoom(self) -> int:
        """Perform a linear search for the room
        with the greatest depth value
        that has an available exit."""
        current = 0
        for id in range(len(self.rooms)):
            if (self.rooms[id].depth > self.rooms[current].depth
                    and len(self.rooms[id].availableExitKeys()) > 0):
                current = id

        return current

    def deepestIncompleteRoomStart(self):
        """Run the depth-first algorithm with initial values."""
        return self.deepestIncompleteRoom([], 0)

    def deepestIncompleteRoom(self, visited: [int], currentID: int):
        """Find the deepest room in the dungeon with a 'None' door.

        Return None if there are no more incomplete rooms.  I probably
        won't  use  this  function,  since  it  produces  over-tangled
        shapes.
        """
        # First, find a deeper room.
        currentRoom = self.rooms[currentID]
        deeperChildren = [currentRoom.exits[exit] for exit in currentRoom.exits
                          if currentRoom.exits[exit] is not None
                          and self.rooms[currentRoom.exits[exit]].depth
                          > currentRoom.depth
                          and currentRoom.exits[exit] not in visited]

        # We would ideally like to avoid visiting the same room again.
        visited.append(currentID)

        # See what the deepest room following each of them is.
        if len(deeperChildren) >= 1:
            # Operate recursively.
            deepCandidates = [self.deepestIncompleteRoom(visited, child)
                              for child in deeperChildren]
            deepCandidates = [candidate for candidate in deepCandidates
                              if candidate is not None]

            if len(deepCandidates) == 1:
                return deepCandidates[0]
            elif len(deepCandidates) > 1:
                # Find the deepest candidate.
                deepest = deepCandidates[0]
                for candidate in deepCandidates[1:]:
                    if self.rooms[candidate].depth > self.rooms[deepest].depth:
                        deepest = candidate
                return deepest
        elif len(currentRoom.availableExitKeys()) > 0:
            # This room has no deeper children,
            # so it is the result if it has an available exit.
            return currentID
        else:
            return None

    def roomsWithEmptyDoors(self) -> [int]:
        """Return a list of IDs for rooms with 'None' doors."""
        available = []
        for roomID in range(len(self.rooms)):
            if len(self.rooms[roomID].availableExitKeys()) > 0:
                available.append(roomID)

        return available

    def nearestRoomWithEmptyDoor(self, head, prev, current, depth):
        """Find the nearest empty door for a cross-link.

        A breadth-first search.
        The head is the room that wants one to connect to.
        """
        if depth <= 0:
            return None

        print("Here we are")
        theRoom = self.rooms[current]
        # If this room has a candidate, use that.
        available = theRoom.availableExitKeys()
        if len(available) > 0:
            return theRoom.exits[available[0]]

        # If one of the ones it connects to has one, use that.
        for room in theRoom.exits:
            if theRoom.exits[room] is not None:
                theirNearest = self.nearestRoomWithEmptyDoor(
                    head, current, theRoom.exits[room], depth - 1)
                if theirNearest is not None:
                    return theirNearest

        # Else, none available.
        return None

    def addRoom(self, previousID: int) -> bool:
        """Add a room to the dungeon, connected to the previous one.
        Return a Boolean indicating whether this succeeded."""
        previous = self.rooms[previousID]

        if len(previous.availableExitKeys()) == 0:
            return False

        connectFrom = random.choice(previous.availableExitKeys())

        self.rooms.append(Room(previous.depth + 1, self,
                               previousID,
                               # Match the directions of the doors.
                               self.door_pairs[connectFrom]))

        # The new room will be at the back.
        self.rooms[previousID].exits[connectFrom] = len(self.rooms) - 1

        return True

    def displayRooms(self):
        """Display the rooms in a dungeon in a readable format."""
        for room in self.rooms:
            print(f"Room {self.rooms.index(room)}: {room.depth}, {room.exits}.")
        print("There are", len(self.rooms), "rooms in this dungeon.")

    def displayRoom(self, roomID):
        """Display info about the room with the given ID."""
        print(f"Room {roomID} has the following exit(s): ", end='')
        for exit in self.rooms[roomID].exits:
            print(f"{exit}, ", end='')
        print("")


class Room:
    """A room in the dungeon."""

    exits: dict[str, int]
    depth: int

    def numDoors(depth: int, layout: RoomLayout) -> int:
        """Generate a number of doors for a room to have at a given depth.
        This will always be at least 1."""
        max_doors_here = math.ceil(layout.max_doors
                                   * (1 - depth/layout.max_depth))
        return max(1, random.randint(max_doors_here - 1, max_doors_here))

    def __init__(self, depth: int, layout: RoomLayout,
                 previous: int, previousDirection):
        """Set up a room, at the given 'depth' (affecting door count),
        connecting to the 'previous' room,
        in the 'previousDirection'."""
        self.exits = {}
        self.depth = depth

        doorCount = Room.numDoors(depth, layout)

        self.exits[previousDirection] = previous

        # We need to choose one door less for the one already added.
        choices = randomChoicesUnique(
            [direction for direction in layout.door_directions
             if direction is not previousDirection], doorCount - 1)

        for door in choices:
            self.exits[door] = None

    def availableExitKeys(self) -> [int]:
        """Return a list of the keys for unused exits."""
        return [key for key in self.exits if self.exits[key] is None]

    def addRandomConnection(self, other: int) -> bool:
        """Add a connection to the other room, if possible,
        via a random exit."""
        options = self.availableExitKeys()
        if len(options) == 0:
            return False

        exit = random.choice(options)
        self.exits[exit] = other
        return True


class Character:
    """A character in the dungeon.
    The behaviour attribute dictates how the character acts in the dungeon.
    It is a reference to a Character method that results in the character
    performing a single action.
    Enemies can wander or fight, while instructions are prompted for
    for player-controlled characters."""

    visited: [int]
    dungeon: Dungeon

    def navigate(self):
        """Navigate the dungeon manually."""

        no_way_message = "There is no exit that way."

        if self.room not in self.visited:
            self.dungeon.layout.displayRoom(self.room)
            self.visited.append(self.room)

        if len(self.dungeon.listOfCharactersInRoom(self.room)) > 1:
            self.dungeon.displayCharactersInRoom(self.room)

        print(f"{self.name} is in room {self.room}, what shall they do?")
        acted = False
        while not acted:
            command = input("> ").lower()

            if len(command) == 0 or command[0] == 'h':
                print(HELP_MESSAGE)
            elif command[0] in self.dungeon.layout.door_directions:
                if self.moveDirection(command[0]):
                    acted = True
                else:
                    print(no_way_message)
            elif command[0] == 'm':
                dir = input("Move where? ").lower()
                if self.moveDirection(dir):
                    acted = True
                else:
                    print(no_way_message)
            elif command[0] == 'l':
                self.dungeon.displayRoom(self.room)
            elif command[0] == 'q':
                global continue_game
                print("Good bye; we hope you return soon!")
                continue_game = False
                acted = True
            else:
                acted = False
                print("Command not understood.")

    def __init__(self, name: str, species: str,
                 health: int, attack: int, room: int,
                 dungeon: Dungeon,
                 behaviour):
        self.name = name
        self.health = health
        self.attack = attack
        self.room = room
        self.species = species
        self.behaviour = behaviour
        self.visited = []
        self.dungeon = dungeon

    def identification(self) -> str:
        """Format a string displaying the character's name and race."""
        return self.name + " the " + self.species

    def wander(self):
        """Wander aimlessly about the dungeon."""
        if randomChance(0.3):
            direction = random.choice(self.dungeon.layout.door_directions)
            self.moveDirection(direction)

    def moveDirection(self, direction) -> bool:
        """Attempt to move and return whether this was achieved."""
        if direction not in self.dungeon.layout.rooms[self.room].exits:
            return False
        else:
            self.room = self.dungeon.layout.rooms[self.room].exits[direction]
            # print(f"{self.identification()} has entered room {self.room}.")
            return True


class Dungeon:
    """The data inherent to the dungeon building."""
    characters: [Character]

    def __init__(self):
        DOOR_PAIRS = {
            "n": "s",
            "e": "w",
            "s": "n",
            "w": "e",
        }

        # Create the room layout
        self.layout = RoomLayout(10, DOOR_PAIRS)

        # Set up the characters
        self.characters = []

        self.characters.append(Character(
            input("Player character name: "), "human", 20, 2, 0,
            self, Character.navigate))

        # Enemies
        for room in self.layout.rooms:
            if randomChance(0.5):
                self.characters.append(
                    Character(names.get_first_name(),
                              random.choice(
                                  ["orc", "goblin", "rat", "snake", "dragon",
                                      "monkey", "human"]),
                              random.randint(10, 20),
                              random.randint(1, 4),
                              self.layout.rooms.index(room),
                              self,
                              Character.wander,
                              ))

    def listOfCharactersInRoom(self, id: int) -> [int]:
        """List the characters in a room."""
        inRoom = []
        for character in self.characters:
            if character.room == id:
                inRoom.append(self.characters.index(character))
        return inRoom

    def displayRoom(self, id: int):
        """Display the exits and contents of the room."""
        self.layout.displayRoom(id)
        self.displayCharactersInRoom(id)

    def displayCharactersInRoom(self, id: int):
        """Display a formatted list of characters in the room."""
        characters_here = self.listOfCharactersInRoom(id)
        if len(characters_here) == 1:
            print(f"{self.characters[characters_here[0]].identification()} is in the room.")
        elif len(characters_here) > 1:
            for i in range(0, len(characters_here)-1):
                print(f"{self.characters[characters_here[i]].identification()}, ", end='')
            print(f"and {self.characters[characters_here[-1]].identification()} are in the room.")

    def run(self):
        """Start simulating the dungeon."""
        while continue_game:
            # print("ROUND START!!!\n")
            for room in self.layout.rooms:
                self.listOfCharactersInRoom(self.layout.rooms.index(room))

            # print("\n\n")
            for character in self.characters:
                character.behaviour(character)


def main():
    """Start a dungeon navigation session."""
    dungeon = Dungeon()
    dungeon.layout.displayRooms()
    print("\n\n\nWELCOME TO THE DUNGEON!")
    print(HELP_MESSAGE)
    dungeon.run()


if __name__ == "__main__":
    main()
